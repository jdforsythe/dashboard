'use strict';

const GH_USER_NOTIFICATIONS_LIMIT = 5;
const GH_USER_EVENTS_LIMIT = 7;

const githubConfig = require('../config').github;

// extend with https://mikedeboer.github.io/node-github/

const bluebird = require('bluebird');
const GitHubApi = require('github');
const github = new GitHubApi({
  // optional
  debug: false,
  protocol: 'https',
  host: 'api.github.com',
  headers: {
    'user-agent': 'Reflection-Project',
  },
  Promise: bluebird,
  followRedirects: false,
  timeout: 5000,
});

github.authenticate({
  type: 'basic',
  username: githubConfig.userName,
  password: githubConfig.password,
});


// API

// user methods
exports.getEventsForUser = getEventsForUser;
exports.getNotifications = getNotifications;
exports.getUser = getUser;

// repo methods
exports.getEventsForRepo = getEventsForRepo;
exports.getHooks = getHooks;
exports.getLatestRelease = getLatestRelease;
exports.getRepo = getRepo;
exports.getStatsCodeFrequency = getStatsCodeFrequency;
exports.getStatsCommitActivity = getStatsCommitActivity;
exports.getStatsParticipation = getStatsParticipation;
exports.getStatsPunchCard = getStatsPunchCard;

// PR methods
exports.getOpenPullRequests = getOpenPullRequests;

// issues methods
exports.getEventsForRepoIssues = getEventsForRepoIssues;
exports.getIssues = getIssues;
exports.getOpenIssues = getOpenIssues;

////////////////////
// exported methods
////////////////////

/**
 * Get events for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getEventsForRepo(userName, repoName) {
  return github.activity.getEventsForRepo({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get events for a repo's issues
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getEventsForRepoIssues(userName, repoName) {
  return github.activity.getEventsForRepoIssues({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get events for a user
 *
 * @param {string} userName - username
 * @returns {Promise}
 */
function getEventsForUser(userName) {
  return github.activity.getEventsForUser({
    user: userName,
    page: 1,
    per_page: githubConfig.limits.userEvents || GH_USER_EVENTS_LIMIT,
  });
}

/**
 * Get hooks for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getHooks(userName, repoName) {
  return github.repos.getHooks({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get the latest release for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getLatestRelease(userName, repoName) {
  return github.repos.getLatestRelease({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get notifications for a user
 *
 * @returns {Promise}
 */
function getNotifications() {
  return github.activity.getNotifications({
    participating: true,
    page: 1,
    per_page: githubConfig.limits.userNotifications || GH_USER_NOTIFICATIONS_LIMIT,
  });
}

/**
 * Get all open issues for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getOpenIssues(userName, repoName) {
  return github.issues.getForRepo({
    user: userName,
    repo: repoName,
    state: 'open',
  });
}

/**
 * Get Issues filtered by specified parameters
 *
 * @param {string} userName
 * @param {string} repoName
 * @param {string} [state=open] - one of 'open', 'closed', 'all'
 * @param {string} [assignee] - username of assignee
 * @param {string} [creator] - username of creator
 * @param {string} [labels] - comma-separated label names (e.g. bug,needs-discussion)
 * @param {string} [mentioned] - username of mentionee
 * @returns {Promise}
 */
function getIssues(userName, repoName, state, assignee, creator, labels, mentioned) {
  const opts = {
    user: userName,
    repo: repoName,
    state: state || 'open',
  };

  if (assignee) opts.assignee = assignee;
  if (creator) opts.creator = creator;
  if (labels) opts.labels = labels;
  if (mentioned) opts.mentioned = mentioned;

  return github.issues.getForRepo(opts);
}

/**
 * Get open pull requests for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getOpenPullRequests(userName, repoName) {
  return github.pullRequests.getAll({
    user: userName,
    repo: repoName,
    state: 'open',
  });
}

/**
 * Get a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getRepo(userName, repoName) {
  return github.repos.get({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get code frequency stats for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getStatsCodeFrequency(userName, repoName) {
  return github.repos.getStatsCodeFrequency({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get commit activity stats for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getStatsCommitActivity(userName, repoName) {
  return github.repos.getStatsCommitActivity({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get participation stats for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getStatsParticipation(userName, repoName) {
  return github.repos.getStatsParticipation({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get hourly stats for a repo
 *
 * @param {string} userName - username for repo
 * @param {string} repoName - repo name
 * @returns {Promise}
 */
function getStatsPunchCard(userName, repoName) {
  return github.repos.getStatsPunchCard({
    user: userName,
    repo: repoName,
  });
}

/**
 * Get a user
 *
 * @param {string} userName - username for repo
 * @returns {Promise}
 */
function getUser(userName) {
  return github.users.getForUser({
    user: userName,
  });
}
