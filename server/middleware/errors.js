'use strict';

module.exports = function(err, req, res, next) {
  if (err)
    res.json({
      message: err.message,
    });
  else
    return next(err, req, res);
};
