'use strict';

const authConfig = require('../../config.js').auth;

module.exports = function(req, res, next) {
  const key = req.get('Authorization').replace('Bearer ', '');

  if (key !== authConfig.token)
    return next(new Error('Unauthorized'));

  return next();
};
