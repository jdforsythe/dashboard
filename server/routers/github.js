'use strict';

const express = require('express');
const router = express.Router();

const github = require('../../github');
const transform = require('../../transforms/github');

////////////////////
// route-specific middleware
////////////////////


////////////////////
// routes
////////////////////
// user routes
router.get('/user/:userName', function(req, res, next) {
  github.getUser(req.params.userName).then(function(user) {
    res.json(transform.user(user));
  }).catch(next);
});

router.get('/user/:userName/events', function(req, res, next) {
  github.getEventsForUser(req.params.userName).then(function(events) {
    res.json(transform.userEvents(events));
  }).catch(next);
});

router.get('/notifications', function(req, res, next) {
  github.getNotifications().then(function(notifications) {
    res.json(transform.userNotifications(notifications));
  }).catch(next);
});

// repo methods
router.get('/repo/:userName/:repoName', function(req, res, next) {
  github.getRepo(req.params.userName, req.params.repoName).then(function(repo) {
    res.json(transform.repo(repo));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/latest-release', function(req, res, next) {
  github.getLatestRelease(req.params.userName, req.params.repoName).then(function(release) {
    res.json(transform.release(release));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/events', function(req, res, next) {
  github.getEventsForRepo(req.params.userName, req.params.repoName).then(function(events) {
    res.json(transform.repoEvents(events));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/stats-code-freq', function(req, res, next) {
  github.getStatsCodeFrequency(req.params.userName, req.params.repoName).then(function(stats) {
    res.json(transform.statsCodeFrequency(stats));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/stats-commit-activity', function(req, res, next) {
  github.getStatsCommitActivity(req.params.userName, req.params.repoName).then(function(stats) {
    res.json(transform.statsCommitActivity(stats));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/stats-participation', function(req, res, next) {
  github.getStatsParticipation(req.params.userName, req.params.repoName).then(function(stats) {
    res.json(transform.statsParticipation(stats));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/stats-punch-card', function(req, res, next) {
  github.getStatsPunchCard(req.params.userName, req.params.repoName).then(function(stats) {
    res.json(transform.statsPunchCard(stats));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/open-pull-requests', function(req, res, next) {
  github.getOpenPullRequests(req.params.userName, req.params.repoName).then(function(prs) {
    res.json(transform.pullRequests(prs));
  }).catch(next);
});

// issues routes
router.get('/repo/:userName/:repoName/issues/events', function(req, res, next) {
  github.getEventsForRepoIssues(req.params.userName, req.params.repoName).then(function(events) {
    res.json(transform.issueEvents(events));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/issues/:state', function(req, res, next) {
  github.getIssues(req.params.userName, req.params.repoName, req.params.state).then(function(issues) {
    res.json(transform.issues(issues));
  }).catch(next);
});

router.get('/repo/:userName/:repoName/issues/:state/:labels', function(req, res, next) {
  github.getIssues(req.params.userName, req.params.repoName, req.params.state, null, null, req.params.labels).then(function(issues) {
    res.json(transform.issues(issues));
  }).catch(next);
});

module.exports = router;
