'use strict';

const express = require('express');
const app = express();

const authMiddleware = require('./server/middleware/auth');
const appErrors = require('./server/middleware/errors');

const githubRoutes = require('./server/routers/github');

////////////////////
// middleware
////////////////////
app.use(authMiddleware);


////////////////////
// routers
////////////////////
app.use('/github', githubRoutes);

////////////////////
// error middleware
////////////////////
app.use(appErrors);

////////////////////
// static routes
////////////////////

app.use(express.static('public'));

////////////////////
// start server
////////////////////

app.listen(3131, function() {
  console.log('Server listening on 3131');
});
