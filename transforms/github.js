'use strict';

const times = require('../times');

exports.user = user;
exports.userEvents = userEvents;
exports.userNotifications = userNotifications;

exports.repo = repo;
exports.hooks = hooks;
exports.release = release;
exports.repoEvents = repoEvents;

exports.statsCodeFrequency = statsCodeFrequency;
exports.statsCommitActivity = statsCommitActivity;
exports.statsParticipation = statsParticipation;
exports.statsPunchCard = statsPunchCard;

exports.pullRequests = pullRequests;

exports.issues = issues;
exports.issueEvents = issueEvents;

////////////////////
// exported methods
////////////////////

function user(u) {
  return {
    realName: u.name,
    email: u.email,
    login: u.login,
    repoCount: u.public_repos,
  };
}

function userNotifications(nots) {
  if (!nots || !nots.count)
    return [];

  return nots.map((n) => {
    return n;
  });
}

function userEvents(evts) {
  if (!evts || !evts.length)
    return [];

  return evts.map((e) => {
    const mapped = {
      login: e.actor.display_login,
      repoName: e.repo.name,
      when: times.getSince(e.created_at),
      type: e.type,
    };

    if (e.type === 'PushEvent') {
      mapped.commitCount = e.payload.commits.length + 1;
      mapped.branch = e.payload.ref.split('/').reverse()[0]; // e.g. "refs/head/branch-name" -> "branch-name"
    }
    else if (e.type === 'PullRequestEvent') {
      mapped.prNumber = e.payload.pull_request.number;
      mapped.action = e.payload.action;
      mapped.mergeability = e.payload.pull_request.mergeable ? 'mergeable' : 'not mergeable';
      mapped.commitCount = e.payload.pull_request.commits;
      mapped.additions = e.payload.pull_request.additions;
      mapped.deletions = e.payload.pull_request.deletions;
    }
    else if (e.type === 'CreateEvent') {
      mapped.ref = e.payload.ref;
      mapped.refType = e.payload.ref_type;
    }
    else if (e.type === 'IssuesEvent') {
      mapped.issueNumber = e.payload.issue.number;
      mapped.action = e.payload.action;
      mapped.title = e.payload.issue.title;
    }
    else if (e.type === 'ForkEvent') {
      mapped.forkedRepo = e.repo.name;
      mapped.forkedTo = e.payload.forkee.full_name;
    }
    else if (e.type === 'IssueCommentEvent') {
      mapped.issueNumber = e.payload.issue.number;
      mapped.comment = e.payload.comment.body;
    }
    else if (e.type === 'DeleteEvent') {
      mapped.ref = e.payload.ref;
      mapped.refType = e.payload.ref_type;
    }
    else {
      mapped.event = e;
    }

    return mapped;
  });
}

function repo(r) {
  return r;
}

function hooks(hks) {
  if (!hks || !hks.length)
    return [];

  return hks.map((h) => {
    return h;
  });
}

function release(rls) {
  return {
    tagName: rls.tag_name,
    version: rls.tag_name.replace('v', ''),
    name: rls.name,
    when: times.getSince(rls.created_at),
  };
}

function repoEvents(evts) {
  return userEvents(evts);
}

function statsCodeFrequency(sts) {
  if (!sts || !sts.length)
    return [];

  return sts.map((s) => {
    return s;
  });
}

function statsCommitActivity(sts) {
  if (!sts || !sts.length)
    return [];

  return sts.map((s) => {
    return s;
  });
}

function statsParticipation(sts) {
  if (!sts || !sts.length)
    return [];

  return sts.map((s) => {
    return s;
  });
}

function statsPunchCard(sts) {
  if (!sts || !sts.length)
    return [];

  return sts.map((s) => {
    return s;
  });
}

function pullRequests(prs) {
  if (!prs || !prs.length)
    return [];

  return prs.map((s) => {
    return s;
  });
}

function issues(iss) {
  if (!iss || !iss.length)
    return [];

  return iss.map((s) => {
    return s;
  });
}

function issueEvents(evts) {
  if (!evts || !evts.length)
    return [];

  return evts.map((e) => {
    return e;
  });
}
