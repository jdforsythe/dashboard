'use strict';

const moment = require('moment');

const NICE_FORMAT = 'MM/DD/YY H:mma';

exports.getNice = getNice;
exports.getSince = getSince;

////////////////////
// exported methods
////////////////////

/**
 * Get a nice, readable date. Without a date argument, returns for the current time
 *
 * @param {Date|string} [uglyDate] - the parsable date (see momentjs)
 * @param {string} [format] - the date format (see momentjs)
 * @returns {string}
 */
function getNice(uglyDate, format) {
  if (uglyDate)
    return moment(uglyDate).format(format || NICE_FORMAT);
  return moment().format(format || NICE_FORMAT);
}

/**
 * Get a nice, from-now readable date, e.g. 2 mins ago
 *
 * @param {Date|string} sinceDate - the parsable comparison date (see momentjs)
 * @param {Boolean} [omitSuffix=false] - whether to omit the suffix e.g. "from now" or "ago"
 * @returns {string}
 */
function getSince(sinceDate, omitSuffix) {
  return moment(sinceDate).fromNow(omitSuffix);
}
