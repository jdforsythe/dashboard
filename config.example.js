'use strict';

module.exports = {
  github: {
    userName: 'GHUSERNAME',
    password: 'GHPASSWORD',
    projectName: 'SenSource-Dashboard',
    limits: {
      userNotifications: 5,
      userEvents: 7,
    },
  },
  auth: {
    token: '123abc',
  },
};
